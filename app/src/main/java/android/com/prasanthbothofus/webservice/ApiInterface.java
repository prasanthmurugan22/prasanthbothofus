package android.com.prasanthbothofus.webservice;

import android.com.prasanthbothofus.model.Events;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by User on 1/24/2018.
 */

public interface ApiInterface {
   String BASE_URL = "https://brottsplatskartan.se/api/events/";

    @GET(BASE_URL)
    Call<Events> getEventByArea(@Query("area")String area);
}
