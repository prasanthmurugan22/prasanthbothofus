package android.com.prasanthbothofus.event_details;

import android.com.prasanthbothofus.R;
import android.com.prasanthbothofus.model.Events;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by User on 1/24/2018.
 */

public class EventDetailsActivity extends AppCompatActivity {
    public static String DATUM = "DATUM";
    TextView txtTitle, txtLocation, txtTime, txtLink, txtDetailDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_detail_activity);
        init();
        setUpDefaults();
        setUpEvents();

    }

    private void init() {
        txtTitle = (TextView) findViewById(R.id.txt_title_Description);
        txtLocation = (TextView) findViewById(R.id.txt_location);
        txtTime = (TextView) findViewById(R.id.txt_time);
        txtDetailDescription = (TextView) findViewById(R.id.txt_detail_description);
        txtLink = (TextView) findViewById(R.id.txt_link);
    }

    private void setUpDefaults() {
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Events.Datum datum = (Events.Datum) getIntent().getSerializableExtra(DATUM);
        if (DATUM != null) {
            String titleText = datum.getTitleType();
            String titleDescription = titleText + " - " + datum.getDescription();
            Spannable spanText = new SpannableString(titleDescription);
            spanText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue_color)), 0, titleText.length(), 0);
            txtTitle.setText(spanText);
            txtLocation.setText(datum.getLocationString());
            txtTime.setText(datum.getDateHuman());
            txtDetailDescription.setText(Html.fromHtml(datum.getContent()));
            txtLink.setText(datum.getExternalSourceLink());
        } else {
            finish();
        }
    }

    private void setUpEvents() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
