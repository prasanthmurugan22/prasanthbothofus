package android.com.prasanthbothofus;

import android.com.prasanthbothofus.event_details.EventDetailsActivity;
import android.com.prasanthbothofus.model.Events;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by User on 1/24/2018.
 */

public class EventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<Events.Datum> datumList;

    public EventAdapter(Context context, List<Events.Datum> datumList) {
        this.context = context;
        this.datumList = datumList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_event_item,parent,false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EventViewHolder eventViewHolder = (EventViewHolder) holder;
        final Events.Datum datum = datumList.get(position);
        String titleText = datum.getTitleType();
        String titleDescription = titleText + " - "+datum.getDescription();
        Spannable spanText = new SpannableString(titleDescription);
        spanText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context,R.color.blue_color)), 0, titleText.length(), 0);
        eventViewHolder.txtTitle.setText(spanText);
        eventViewHolder.txtLocation.setText(datum.getLocationString());
        eventViewHolder.txtTime.setText(datum.getDateHuman());
        eventViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EventDetailsActivity.class);
                intent.putExtra(EventDetailsActivity.DATUM,datum);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void updateList(List<Events.Datum> datumList){
        this.datumList = datumList;
        notifyDataSetChanged();
    }

    private class EventViewHolder extends RecyclerView.ViewHolder{
        TextView txtTitle,txtLocation,txtTime;
        public EventViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_title_Description);
            txtLocation = (TextView) itemView.findViewById(R.id.txt_location);
            txtTime = (TextView) itemView.findViewById(R.id.txt_time);
        }
    }
}
