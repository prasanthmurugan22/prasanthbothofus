package android.com.prasanthbothofus.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Events implements Serializable {

    @SerializedName("links")
    @Expose
    private Links links;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Links  implements Serializable{

        @SerializedName("current_page")
        @Expose
        private Integer currentPage;
        @SerializedName("first_page_url")
        @Expose
        private String firstPageUrl;
        @SerializedName("from")
        @Expose
        private Integer from;
        @SerializedName("last_page")
        @Expose
        private Integer lastPage;
        @SerializedName("last_page_url")
        @Expose
        private String lastPageUrl;
        @SerializedName("next_page_url")
        @Expose
        private String nextPageUrl;
        @SerializedName("path")
        @Expose
        private String path;
        @SerializedName("per_page")
        @Expose
        private Integer perPage;
        @SerializedName("prev_page_url")
        @Expose
        private Object prevPageUrl;
        @SerializedName("to")
        @Expose
        private Integer to;
        @SerializedName("total")
        @Expose
        private Integer total;

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        public String getFirstPageUrl() {
            return firstPageUrl;
        }

        public void setFirstPageUrl(String firstPageUrl) {
            this.firstPageUrl = firstPageUrl;
        }

        public Integer getFrom() {
            return from;
        }

        public void setFrom(Integer from) {
            this.from = from;
        }

        public Integer getLastPage() {
            return lastPage;
        }

        public void setLastPage(Integer lastPage) {
            this.lastPage = lastPage;
        }

        public String getLastPageUrl() {
            return lastPageUrl;
        }

        public void setLastPageUrl(String lastPageUrl) {
            this.lastPageUrl = lastPageUrl;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public Integer getPerPage() {
            return perPage;
        }

        public void setPerPage(Integer perPage) {
            this.perPage = perPage;
        }

        public Object getPrevPageUrl() {
            return prevPageUrl;
        }

        public void setPrevPageUrl(Object prevPageUrl) {
            this.prevPageUrl = prevPageUrl;
        }

        public Integer getTo() {
            return to;
        }

        public void setTo(Integer to) {
            this.to = to;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

    }

    public class Datum  implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("pubdate_iso8601")
        @Expose
        private String pubdateIso8601;
        @SerializedName("pubdate_unix")
        @Expose
        private String pubdateUnix;
        @SerializedName("title_type")
        @Expose
        private String titleType;
        @SerializedName("title_location")
        @Expose
        private String titleLocation;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("content_formatted")
        @Expose
        private String contentFormatted;
        @SerializedName("content_teaser")
        @Expose
        private String contentTeaser;
        @SerializedName("location_string")
        @Expose
        private String locationString;
        @SerializedName("date_human")
        @Expose
        private String dateHuman;
        @SerializedName("lat")
        @Expose
        private Double lat;
        @SerializedName("lng")
        @Expose
        private Double lng;
        @SerializedName("viewport_northeast_lat")
        @Expose
        private String viewportNortheastLat;
        @SerializedName("viewport_northeast_lng")
        @Expose
        private String viewportNortheastLng;
        @SerializedName("viewport_southwest_lat")
        @Expose
        private String viewportSouthwestLat;
        @SerializedName("viewport_southwest_lng")
        @Expose
        private String viewportSouthwestLng;
        @SerializedName("administrative_area_level_1")
        @Expose
        private String administrativeAreaLevel1;
        @SerializedName("administrative_area_level_2")
        @Expose
        private Object administrativeAreaLevel2;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("external_source_link")
        @Expose
        private String externalSourceLink;
        @SerializedName("permalink")
        @Expose
        private String permalink;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPubdateIso8601() {
            return pubdateIso8601;
        }

        public void setPubdateIso8601(String pubdateIso8601) {
            this.pubdateIso8601 = pubdateIso8601;
        }

        public String getPubdateUnix() {
            return pubdateUnix;
        }

        public void setPubdateUnix(String pubdateUnix) {
            this.pubdateUnix = pubdateUnix;
        }

        public String getTitleType() {
            return titleType;
        }

        public void setTitleType(String titleType) {
            this.titleType = titleType;
        }

        public String getTitleLocation() {
            return titleLocation;
        }

        public void setTitleLocation(String titleLocation) {
            this.titleLocation = titleLocation;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getContentFormatted() {
            return contentFormatted;
        }

        public void setContentFormatted(String contentFormatted) {
            this.contentFormatted = contentFormatted;
        }

        public String getContentTeaser() {
            return contentTeaser;
        }

        public void setContentTeaser(String contentTeaser) {
            this.contentTeaser = contentTeaser;
        }

        public String getLocationString() {
            return locationString;
        }

        public void setLocationString(String locationString) {
            this.locationString = locationString;
        }

        public String getDateHuman() {
            return dateHuman;
        }

        public void setDateHuman(String dateHuman) {
            this.dateHuman = dateHuman;
        }

        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        public Double getLng() {
            return lng;
        }

        public void setLng(Double lng) {
            this.lng = lng;
        }

        public String getViewportNortheastLat() {
            return viewportNortheastLat;
        }

        public void setViewportNortheastLat(String viewportNortheastLat) {
            this.viewportNortheastLat = viewportNortheastLat;
        }

        public String getViewportNortheastLng() {
            return viewportNortheastLng;
        }

        public void setViewportNortheastLng(String viewportNortheastLng) {
            this.viewportNortheastLng = viewportNortheastLng;
        }

        public String getViewportSouthwestLat() {
            return viewportSouthwestLat;
        }

        public void setViewportSouthwestLat(String viewportSouthwestLat) {
            this.viewportSouthwestLat = viewportSouthwestLat;
        }

        public String getViewportSouthwestLng() {
            return viewportSouthwestLng;
        }

        public void setViewportSouthwestLng(String viewportSouthwestLng) {
            this.viewportSouthwestLng = viewportSouthwestLng;
        }

        public String getAdministrativeAreaLevel1() {
            return administrativeAreaLevel1;
        }

        public void setAdministrativeAreaLevel1(String administrativeAreaLevel1) {
            this.administrativeAreaLevel1 = administrativeAreaLevel1;
        }

        public Object getAdministrativeAreaLevel2() {
            return administrativeAreaLevel2;
        }

        public void setAdministrativeAreaLevel2(Object administrativeAreaLevel2) {
            this.administrativeAreaLevel2 = administrativeAreaLevel2;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getExternalSourceLink() {
            return externalSourceLink;
        }

        public void setExternalSourceLink(String externalSourceLink) {
            this.externalSourceLink = externalSourceLink;
        }

        public String getPermalink() {
            return permalink;
        }

        public void setPermalink(String permalink) {
            this.permalink = permalink;
        }

    }

}