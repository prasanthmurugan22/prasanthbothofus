package android.com.prasanthbothofus;

import android.app.ProgressDialog;
import android.com.prasanthbothofus.model.Events;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    int DELAY = 2000;
    EditText edtCityName;
    RecyclerView rvEvents;
    List<Events.Datum> datumList = new ArrayList<>();
    EventAdapter eventAdapter;
    TextView txtEmpltyView;
    Handler handler;
    Runnable runnable;
    String strCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setUpDefaults();
        setUpEvents();
    }

    private void init() {
        edtCityName = (EditText) findViewById(R.id.edt_city_name);
        rvEvents = (RecyclerView) findViewById(R.id.rv_events);
        txtEmpltyView = (TextView) findViewById(R.id.txt_empty);
    }

    private void setUpDefaults() {
        eventAdapter = new EventAdapter(this, datumList);
        rvEvents.setLayoutManager(new LinearLayoutManager(this));
        rvEvents.setAdapter(eventAdapter);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                getEventBasedOnArea(strCity);
            }
        };
    }

    private void setUpEvents() {
        edtCityName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                strCity = charSequence.toString();
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable,DELAY);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edtCityName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    handler.removeCallbacks(runnable);
                    getEventBasedOnArea(edtCityName.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }

    private void getEventBasedOnArea(String area) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        getBaseApplication().getApiInterface().getEventByArea(area).enqueue(new Callback<Events>() {
            @Override
            public void onResponse(Call<Events> call, Response<Events> response) {
                progressDialog.dismiss();
                Events events = response.body();
                if (events != null && events.getData().size() > 0) {
                    rvEvents.setVisibility(View.VISIBLE);
                    txtEmpltyView.setVisibility(View.GONE);
                    datumList = events.getData();
                    eventAdapter.updateList(datumList);
                } else {
                    rvEvents.setVisibility(View.GONE);
                    txtEmpltyView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Events> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private BothOfUsApplication getBaseApplication() {
        return (BothOfUsApplication) getApplication();
    }
}
